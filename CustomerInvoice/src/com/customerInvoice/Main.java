package com.customerInvoice;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Invoice> a = new ArrayList<>();
        ArrayList<Product> product1 = new ArrayList<>();
        product1.add(new Product("Shoe",111,700));
        product1.add(new Product("Shirt",112,1200));

        ArrayList<Product> product2 = new ArrayList<>();
        product2.add(new Product("Watch",11,1500));
        product2.add(new Product("Pant",222,800));

        Invoice in1 = new Invoice(1,product1,new Customer(111,"Sagor","Mohamadpur"));
        Invoice in2 = new Invoice(2,product2,new Customer(222,"Rahman","Savar"));

        a.add(in1);
        a.add(in2);

        for(int i=0; i<a.size(); i++){
            System.out.println("Invoice No: "+a.get(i).getInvoiceNo());
            System.out.println("Customer Id: "+a.get(i).getCustomerId());
            System.out.println("Customer Name: "+a.get(i).getCustomerName());
            System.out.println("Customer Address: "+a.get(i).getCustomerAddress());
            System.out.println();
            for(int j=0; j<a.get(i).pd.size(); j++){
                System.out.println("Product "+(j+1)+ " name: "+a.get(i).pd.get(j).getProductName());
                System.out.println("Product "+(j+1)+ " Id: "+a.get(i).pd.get(j).getProductId());
                System.out.println("Product "+(j+1)+ " Price: "+a.get(i).pd.get(j).getPrice());
                System.out.println();
            }

        }



    }



}
