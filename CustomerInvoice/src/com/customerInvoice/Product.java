package com.customerInvoice;

public class Product {
    private String productName;
    private int productId;
    private int price;

    public Product(String productName, int productId, int price){
        this.productName = productName;
        this.productId = productId;
        this.price = price;
    }
    public String getProductName() {
        return productName;
    }
    public void setProductName(String pName) {
        this.productName = productName;
    }
    public int getProductId() {
        return productId;
    }
    public void setProductId(int pId) {
        this.productId = productId;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
}

