package com.customerInvoice;

import java.util.ArrayList;

public class Invoice {
    private int invoiceNo;
    ArrayList<Product> pd = new ArrayList<>();
    Customer C;
     public Invoice(int invoiceNo, ArrayList<Product> pd, Customer cus){
         this.invoiceNo = invoiceNo;
         this.pd = pd;
         C = cus;
     }

    public int getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(int invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public ArrayList<Product> getPd() {
        return pd;
    }

    public void setP(ArrayList<Product> pd) {
        this.pd = pd;
    }

    public Customer getC() {
        return C;
    }

    public void setC(Customer cus) {
        C = cus;
    }

    public String getCustomerName(){
        return C.getCustomerName();
    }
    public int getCustomerId(){
        return C.getCustomerId();
    }
    public String getCustomerAddress(){
        return C.getCustomerAddress();
    }



}
